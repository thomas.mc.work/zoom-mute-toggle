#!/usr/bin/env sh
# toggle the mute status in some video conference solutions
# requires xdotool:
#   apt install xdotool

notify() {
  aplay '/usr/share/sounds/sound-icons/percussion-10.wav'
}

bbb=$(xdotool search --limit 1 --name "BigBlueButton - .* — .*")
if [ $? -eq 0 ]; then
  [ -n "$DEBUG" ] && notify-send "BBB found"
  xdotool key --window $bbb --clearmodifiers "alt+shift+m"
  notify
fi

jitsi=$(xdotool search --limit 1 --name " \| Jitsi Meet ")
if [ $? -eq 0 ]; then
  [ -n "$DEBUG" ] && notify-send "Jitsi Meet found"
  xdotool key --window $jitsi --clearmodifiers "m"
  notify
fi

ms_teams=$(xdotool search --limit 1 --name "Microsoft Teams classic ")
if [ $? -eq 0 ]; then
  [ -n "$DEBUG" ] && notify-send "Microsoft Teams found"
  current=$(xdotool getwindowfocus)
  xdotool windowactivate --sync ${ms_teams}
  # sometimes doesn't work without a delay
  sleep 0.1
  xdotool key --clearmodifiers "ctrl+shift+m"
  xdotool windowactivate --sync ${current}
  notify
fi

zoom=$(xdotool search --limit 1 --name "Zoom Meeting")
if [ $? -eq 0 ]; then
  [ -n "$DEBUG" ] && notify-send "Zoom found"
  current=$(xdotool getwindowfocus)
  xdotool windowactivate --sync ${zoom}
  # sometimes doesn't work without a delay
  sleep 0.1
  xdotool key --clearmodifiers "alt+a"
  xdotool windowactivate --sync ${current}
  notify
fi

