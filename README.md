# Mute toggle for video conferences

This script allows you to toggle the mute status of some video conference solutions from any other window. This is particularly useful to be used with a keyboard shortcut.

- Zoom desktop client
- Jitsi Meet
- BigBlueButton

## Requirements

- xdotool

## Setup

    apt install xdotool
    mkdir -p ~/.local/bin
    wget -O ~/.local/bin/vc-mute-toggle.sh "https://gitlab.com/thomas.mc.work/vc-mute-toggle/-/raw/master/vc-toggle-mute.sh?inline=false"
    chmod +x ~/.local/bin/vc-mute-toggle.sh

You might be required to add `~/.local/bin` to your `PATH` variable (usually done in `~/.profile`: `PATH="$PATH:$HOME/.local/bin`)

## Global shortcut

I recommend to use this script with a keyboard shortcut, e.g. <kbd>Super</kbd> + <kbd>s</kbd>.

### Setup on XFCE

    xfconf-query -c xfce4-keyboard-shortcuts -p '/commands/custom/<Super>s' -nt string -s 'zoom-mute-toggle.sh'

## Credits

This scrip this mostly based on this blog post:

<https://pzel.name/til/2019/11/25/Muting-and-unmuting-Zoom-from-anywhere-on-the-linux-desktop.html>

Thank you, Simon!